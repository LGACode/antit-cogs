import discord

from datetime import date, datetime

from redbot.core import commands
from redbot.core.i18n import Translator, cog_i18n

_ = Translator("ExampleCog", __file__)

@cog_i18n(_)
class Timers(commands.Cog):
    # Credits in the README
    """
    Timers for some events
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def timers(self, ctx):
        """Differents timers of different event."""
        pass

    @timers.command()
    async def christmas(self, ctx):
        """Christam 🎁 Timer !"""

        now = datetime.now()
        today = date(now.year, now.month, now.day)

        year = now.year
        if (now.month == 12 and now.day > 25):
            year = now.year + 1

        christmasday = date(year, 12, 25)
        delta = christmasday - today

        try:
            e = make_embed(_("Christmas"), 25, 12, "🎁", delta.days, 15158332)
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(str(delta.days) + _(" days left until Christmas 🎁"))

    @timers.command()
    async def halloween(self, ctx):
        """Halloween 🎃 Timer !"""

        delta = return_delta(31, 10)

        try:
            e = make_embed(_("Halloween"), 31, 10, "🎃", delta, 16746268)
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(str(delta) + _(" days left until Halloween 🎃"))

    @timers.command()
    async def newyear(self, ctx):
        """New Year 🎊 Timer !"""

        delta = return_delta(1, 1)

        try:
            e = make_embed(_("the new year"), 1, 1, "🎊", delta, 15592721)
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(str(delta) + _(" days left until the new year 🎊"))

    @timers.command()
    async def stvalentine(self, ctx):
        """St Valentine Day 💗 Timer !"""

        delta = return_delta(14, 2)

        try:
            e = make_embed(_("St Valentine"), 14, 2, "💗", delta, 14230878)
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(str(delta) + _(" days left until St Valentine 💗"))

    @timers.command()
    async def stpatrick(self, ctx):
        """St Patrick 🍀 Timer !"""

        delta = return_delta(17, 3)

        try:
            e = make_embed(_("St Patrick"), 17, 3, "🍀", delta, 3120702)
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(str(delta) + _(" days left until the St Patrick 🍀"))

    @timers.command()
    async def aprilfool(self, ctx):
        """April Fool 🐰 Timer !"""

        delta = return_delta(1, 4)

        try:
            e = make_embed(_("April fool"), 1, 4, "🐰", delta, 9408399)
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(str(delta) + _(" days left until April fool**s** 🐰"))

def return_delta(day, month):
    now = datetime.now()
    today = date(now.year, now.month, now.day)
    year = now.year

    if now.month > month or (now.month == month and now.day > day):
        year = now.year + 1

    return_date = date(year, month, day)
    delta = return_date - today

    return delta.days

def make_embed(name, day, month, emoji, delta, color):
    e = discord.Embed(description=_("Countdown to {}").format(name), color=color)
    e.add_field(name=_("Time left until {} *({}/{})* {}").format(name, day, month, emoji), value=str(delta) + _(" days left."))
    return e
