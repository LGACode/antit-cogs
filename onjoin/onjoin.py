import discord

from redbot.core import checks, commands, Config
from redbot.core.i18n import Translator, cog_i18n

_ = Translator("ExampleCog", __file__)

@cog_i18n(_)
class OnJoin(commands.Cog):
    """Send a message to the guild owner when Red join a new server."""
    def __init__(self, bot):
        self.config = Config.get_conf(self, identifier=99884680)
        self.bot = bot
        self.config.register_global(messsage=None)
        
    @commands.group()
    @checks.is_owner()
    async def onjoin(self, ctx):
        """Send a message to the guild owner who invited Red"""
        pass

    @onjoin.command()
    async def setmessage(self, ctx, *, message=None):
        """Set the message you want to send.

        This message will be sended to the guild owner when he invite the bot to his server."""

        if message is None:
            await self.config.clear_all()
            return await ctx.send("The message has been cleared.")

        await self.config.message.set(message)
        e = discord.Embed(color=await ctx.embed_colour(), description=":white_check_mark: The message has been set.")
        try:
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(":white_check_mark: The message has been set.")

    @onjoin.command()
    async def message(self, ctx):
        """Return the set message."""
        message = await self.config.message()
        
        if message is None:
            return await ctx.send("No message has been set.")

        e = discord.Embed(color=await ctx.embed_colour(), title="The message that you set is :", description=message)
        try:
            await ctx.send(embed=e)
        except discord.HTTPException:
            await ctx.send(message)

    @commands.Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        try:
            owner = self.bot.get_user(guild.owner_id)
            message = await self.config.message()
            if message is None:
                return
            try:
                await owner.send(message)
            except discord.HTTPExcption:
                pass
        except Exception:
            pass
