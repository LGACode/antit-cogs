<h1 id="antitcogs">AntiT-Cogs</h1>
<p>Some cogs for Red Discord Bot v3, by Sinnoma. Some has been made by other and has been rewritten/improved. Credits availaible at the end.</p>
<h2 id="availaiblecogs">Availaible Cogs</h2>
<h3 id="onjoin">OnJoin</h3>
<ul>
<li><strong>Working :</strong> Send a message to the guild owner when the bot join a server.</li>
</ul>
<h3 id="littletools">LittleTools</h3>
<ul>
<li><strong>Working :</strong> Some commands for your bot. Random commands from my mind. Has a christmas counter, react command, a link shortner (Need a API key from rebrandly), a file saver command, and a command to send links of a bot.</li>
</ul>
<h3 id="timers">Timers</h3>
<ul>
<li><strong>Cog Finished :</strong> Timers of special events.</li>
</ul>
<h3 id="lyrics">Lyrics</h3>
<ul>
<li><strong>Working :</strong> Lyrics of musics.</li>
</ul>
<h3 id="lyrics">CategoryDeleter</h3>
<ul>
<li><strong>Beta Open :</strong> Delete all channels of a category easily. </li>
</ul>
<h1 id="credits">Credits</h1>
<ul>
<li><strong>Timers :</strong> <em>UltimatePancake</em> (Repo : https://github.com/UltimatePancake/Pancake-Cogs/blob/master/xmasclock/xmasclock.py)</li>
</ul>
