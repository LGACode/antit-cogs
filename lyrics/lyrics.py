import discord
import aiohttp

from redbot.core import commands
from redbot.core import __version__ as version
from redbot.core.utils.chat_formatting import pagify
from redbot.core.i18n import Translator, cog_i18n

_ = Translator("ExampleCog", __file__)

API_ERROR = ("API Key for KSoft.Si isn't registered, "
    "use `[p]set api ksoft api_key,<Your_API_KEY>` to"
    " be able to use the command.\nIf you're not the"
    " bot owner, use `[p]contact` and report that"
    " there's not API key registered.")

@cog_i18n(_)
class Lyrics(commands.Cog):
    """Lyrics."""

    def __init__(self, bot):
        self.bot = bot
        self._session = aiohttp.ClientSession()

    @commands.command()
    async def lyrics(self, ctx, *, song = None):

        # Checking if the API key is registered.
        apikey = await self.bot.db.api_tokens.get_raw('ksoft', default={'api_key': None})
        if apikey['api_key'] is None:
            return await ctx.send(API_ERROR)
        
        if song is None:
            return await ctx.send_help()

        # Research
        headers = {'Authorization': 'NANI ' + str(apikey['api_key']), 'user-agent': 'Red-DiscordBot/' + version}
        params = {'q': str(song), 'limits': 1}
        await ctx.send("I'm searching the lyrics of your music, give me a seconds...")

        async with aiohttp.ClientSession() as session:
            async with session.get("https://api.ksoft.si/lyrics/search", headers=headers, params=params) as response:
                try:
                    json = await response.json()
                    lyrics = json['data'][0]['lyrics']
                    music_name = json['data'][0]['name']
                    music_cover = json['data'][0]['album_art']
                except Exception:
                    return await ctx.send("Error while parsing lyrics.")

                # Sending the research
                for page in pagify(lyrics):
                    e = discord.Embed(title="*__{}__*".format(music_name),description=page,color=await ctx.embed_colour())
                    e.set_footer(text="Powered by KSoft.Si.")
                    e.set_author(name="Requested by {}".format(ctx.author.name), icon_url=ctx.author.avatar_url)
                    e.set_thumbnail(url=music_cover)
                    await ctx.send(embed=e)
