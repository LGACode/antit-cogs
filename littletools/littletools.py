import discord
import aiohttp
import json

from io import BytesIO

from redbot.core import commands, checks, Config
from redbot.core.i18n import Translator, cog_i18n

_ = Translator("Leveler", __file__)

@cog_i18n(_)
class LittleTools(commands.Cog):
    """Some little useful (Or not) tools."""

    def __init__(self, bot):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=99884680)
        self._session = aiohttp.ClientSession()

        default_global = {
          "rebrandlyapi": None,
        }

        self.config.register_global(**default_global)

    def cog_unload(self):
        self.bot.loop.create_task(self._session.close())

    @commands.group()
    @checks.is_owner()
    async def apiset(self, ctx):
        """Register your API key here."""
        # I could add more API, so making a group was useful if needed.
        pass

    @apiset.command()
    async def rebrandly(self, ctx, api:str = None):
        """Register the API key for link shortner using Rebrandly

        How to guide:
        1. Connect to your rebrandly account (https://app.rebrandly.com/login).
        2. Click on your profile and choose `API Keys`.
        3. Create a key and insert it here using `[p]apiset rebrandly <Your token>`."""
        if await self.config.rebrandlyapi() is None:
            return await ctx.send_help()
        elif api is None: 
            return await ctx.send("Your key has been cleared.")

        await self.config.rebrandlyapi.set(api)
        await ctx.send("API key for Rebrandly set.")

    @commands.command()
    async def save(self, ctx):
        """
        Save your file at anonymousfile.io
        The command must include a attachment.
        """
        if not ctx.message.attachments:
            await ctx.send_help()
        for attachment in ctx.message.attachments:
            try:
                fp = BytesIO()

                message = await ctx.send("Starting downloading... Please wait, this can take a while depending of my Internet connection.")
                await attachment.save(fp)
                fp.seek(0)
                await message.edit(content="Downloaded, uploading... Please wait, this can take a while depending of my Internet connection.")

                async with aiohttp.ClientSession() as session:
                    async with session.post('https://api.anonymousfiles.io', data={'file': fp}) as r:
                        link = await r.json()
                        await ctx.author.send("Your file has been uploaded : " + link['url'])
                        await message.edit(content="File uploaded.")
            except Exception:
                await message.edit(content="Something was wrong during operation.")

    @commands.command()
    async def short(self, ctx, *, url:str):
        """Shorten your URL. 

        Include a valid URL."""

        if url.startswith("https://" or "http://" or "www."):
            pass
        else:
            return await ctx.send("This is not a valid link. It must start with `https`, `http` or `www`.")

        linkRequest = {"destination": url, "domain": { "fullName": "rebrand.ly" }}
        requestHeaders = {"Content-type": "application/json", "apikey": await self.config.rebrandlyapi()}
        message = await ctx.send("Working...")

        try:
            async with aiohttp.ClientSession() as session:
                async with session.post('https://api.rebrandly.com/v1/links', data=json.dumps(linkRequest), headers=requestHeaders) as r:
                    link = await r.json()
                    await ctx.author.send("Your link is available here : https://" + link['shortUrl'])
                    await message.edit(content="Done.")

        except ContentTypeError:
            await message.edit(content="Error, the owner set an invalid API key, contact him and report the issue.")
        except KeyError:
            await message.edit(content="Well, your link wasn't valid, try with an another link and/or valid one.")

    @commands.command(cooldown_after_parsing=True)
    @commands.cooldown(2, 60, commands.BucketType.user)
    async def bot(self, ctx, *, bot_id:int):
        """Send 2 links of a bot using his ID.

        Cooldown : Usable 2 time every 1 minute."""
        link = "(https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions={})"

        if 17 > len(str(bot_id)) or len(str(bot_id)) > 19:
            return await ctx.send("This is not a valid ID.")

        try:
            bot = await self.bot.fetch_user(bot_id)
        except Exception as e:
            return await ctx.send("Cannot fetch ID : {}".format(str(e)))

        if bot.bot is False:
            return await ctx.send("This is an user, try again with a bot ID.")

        e = discord.Embed(color=await ctx.embed_colour(), description="{} Invite Links".format(bot.name))
        e.add_field(name="Basic permissions (3263552)", value="[Link to the OAuth2]" + link.format(bot.id, "3263552"))
        e.add_field(name="No permissions (No role created) (0)", value="[Link to the OAuth2]" + link.format(bot.id, "0"))
        e.set_thumbnail(url=bot.avatar_url)
        e.set_author(name="Requested by {}".format(ctx.author),icon_url=ctx.author.avatar_url)
        try:
            await ctx.send(embed=e)
        except discord.HTTPException:
            try:
                await ctx.author.send(embed=e)
                await ctx.send("Cannot send in the server, I send it in your DM")
            except discord.HTTPException:
                await ctx.send("Cannot send on the server or in your DM, give me the `Embed Links` permission or unlock your private message.")
