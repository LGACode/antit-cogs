import discord

from redbot.core import commands, checks
from redbot.core.utils.predicates import MessagePredicate

class CategoryDeleter(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @checks.bot_has_permissions(manage_channels = True)
    @checks.admin_or_permissions(manage_channels = True)
    async def deletecategory(self, ctx, *, category: discord.CategoryChannel = None):
        if category == None:
            return await ctx.send_help()

        await ctx.send("Are you sure to delete every channels in {} ? (Yes/No) **(THIS CANNOT BE UNDONE!)**".format(category.name))
        pred = MessagePredicate.yes_or_no(ctx)
        await self.bot.wait_for("message", check=pred)
        if pred.result is True:
            message = await ctx.send("Deleting channels...")
            for all_channels in category.channels:
                try:
                    await all_channels.delete(reason="Requested by {}.".format(ctx.author.name))
                except discord.HTTPException:
                    await ctx.send("Error while deleting channels.")
            await ctx.send("All channels has been deleted.")
        else:
            return await ctx.send("Cancelled.")

def setup(bot):
    cog = CategoryDeleter(bot)
    bot.add_cog(cog)
